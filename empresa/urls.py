from django.conf.urls import patterns, include, url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^sucursales/$', views.sucursales, name="sucursales"),
    url(r'^sucursales/nueva/$', views.nueva_sucursal, name="nueva_sucursal"),
    url(r'^sucursales/(?P<id>[\d]+)/editar/$', views.editar_sucursal, name="editar_sucursal"),
    url(r'^empleados/$', views.empleados, name="empleados"),
    url(r'^empleado/nuevo/$', views.nuevo_empleado_sucursal, name="nuevo_empleado_sucursal"),
    url(r'^empleado/(?P<id>[\d]+)/nuevo$', views.nuevo_empleado, name='nuevo_empleado'),
    url(r'^empleado/(?P<id>[\d]+)/editar/$', views.editar_empleado, name="editar_empleado"),
]
