from django.db import models
from django.contrib.auth.models import User

class Sucursal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length = 100)
    calle = models.CharField(max_length = 50)
    colonia = models.CharField(max_length = 50)
    numero_exterior = models.IntegerField()
    numero_interior = models.IntegerField()
    ciudad = models.CharField(max_length = 50)
    pais = models.CharField(max_length = 100)

    def __str__(self):
        return self.nombre

class Empleado(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length = 100)
    rfc = models.CharField(max_length = 50)
    puesto = models.CharField(max_length = 50)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre
