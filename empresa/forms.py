from django import forms
from django.forms import ModelForm
from .models import Sucursal, Empleado

class SucursalForm(forms.ModelForm):
    class Meta:
        model = Sucursal
        fields = '__all__'
        exclude = ['user']

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        exclude = ['sucursal', 'user']

class EmpleadoSucursalForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields = '__all__'
        exclude = ['user']
