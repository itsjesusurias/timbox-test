from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from .models import Sucursal, Empleado
from .forms import SucursalForm, EmpleadoForm, EmpleadoSucursalForm
from django.db.models import Count


@login_required()
def index(request):
    current_user = request.user
    sucursales = Sucursal.objects.filter(user=current_user).annotate(empleados=Count('empleado'))
    return render(request,'sucursales/index.html', locals())

@login_required()
def sucursales(request):
    current_user = request.user
    sucursales = Sucursal.objects.filter(user=current_user).annotate(empleados=Count('empleado'))
    return render(request,'sucursales/index.html', locals())

@login_required()
def empleados(request):
    current_user = request.user
    empleados = Empleado.objects.filter(user=current_user).all()
    return render(request,'empleados/index.html', locals())

@login_required()
def nueva_sucursal(request):
    current_user = request.user
    if request.method == "POST":
        form = SucursalForm(request.POST)
        if form.is_valid():
            sucursal = form.save(commit=False)
            sucursal.user = current_user
            sucursal.save()
            return redirect('/empleado/%d/nuevo' % sucursal.pk)
    else:
        form = SucursalForm()
    return render(request,'sucursales/nueva.html', locals())

@login_required()
def editar_sucursal(request, id):
    sucursal_edit = get_object_or_404(Sucursal, pk=id)
    es_edicion = False if sucursal_edit is None else True
    if request.method == "POST":
        form = SucursalForm(request.POST, instance=sucursal_edit)
        if form.is_valid():
            sucursal = form.save()
            return redirect(reverse('empresa:index'))
    else:
        form = SucursalForm(instance=sucursal_edit)
    return render(request,'sucursales/nueva.html', locals())

@login_required()
def nuevo_empleado(request, id):
    current_user = request.user
    if request.method == "POST":
        form = EmpleadoForm(request.POST)
        if form.is_valid():
            empleado = form.save(commit=False)
            empleado.user = current_user
            empleado.sucursal_id = id
            empleado.save()
            return redirect('/empleado/%d/nuevo' % int(id))
    else:
        form = EmpleadoForm()
    return render(request,'empleados/nuevo.html', locals())

@login_required()
def nuevo_empleado_sucursal(request):
    current_user = request.user
    if request.method == "POST":
        form = EmpleadoSucursalForm(request.POST)
        if form.is_valid():
            empleado = form.save(commit=False)
            empleado.user = current_user
            empleado.save()
            return redirect(reverse('empresa:empleados'))
    else:
        form = EmpleadoSucursalForm()
    return render(request,'empleados/nuevo.html', locals())

@login_required()
def editar_empleado(request, id):
    empleado_edit = get_object_or_404(Empleado, pk=id)
    es_edicion = False if empleado_edit is None else True
    if request.method == "POST":
        form = EmpleadoForm(request.POST, instance=empleado_edit)
        if form.is_valid():
            empleado = form.save()
            return redirect(reverse('empresa:index'))
    else:
        form = EmpleadoForm(instance=empleado_edit)
    return render(request,'empleados/nuevo.html', locals())
