var common = (function () {

    var isNullOrEmpty = function (fields) {
        for (var i = 0;i < fields.length; i++) {
            var field = fields[i];
            if (field && !field.val()) {
                field.addClass('error');
            }
            else if (field){
                field.removeClass('error');
            }
        }
    };

    var isNumber = function (fields) {
        for (var i = 0;i < fields.length; i++) {
            var field = fields[i];
            field.val(field.val().replace(/[^0-9]/g, ''));
        }
    };

    var showErrors = function (element, message) {
        element.html(message);
    };

    return {
        isNullOrEmpty: isNullOrEmpty,
        isNumber: isNumber,
        showErrors: showErrors,
    };

})();
