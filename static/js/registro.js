var registro = (function () {

    var init = function() {
        $('#registrar').on('click', validateFields);
    };

    var validateFields = function(event) {

        isValid(function (argument) {
            $form = $('#register_form');
            if ($form.find('.error').length <= 0) {
                $form.submit();
            }else{
                showErrors();
            }
        });
    };

    var isValid = function (callback) {
        var $nombre = $('#id_nombre_completo'),
            $correo = $('#id_email'),
            $rfc = $('#id_rfc'),
            $empresa = $('#id_empresa'),
            $contrasena = $('#id_password'),
            $contrasenaConfirm = $('#password-repeat');
        common.isNullOrEmpty([$nombre, $correo, $rfc, $empresa, $contrasena, $contrasenaConfirm]);
        itEquals($contrasena, $contrasenaConfirm);

        callback();
    }

    var isNullOrEmpty = function (fields) {
        for (var i = 0;i < fields.length; i++) {
            var field = fields[i];
            if (field && !field.val()) {
                field.addClass('error');
            }
            else if (field){
                field.removeClass('error');
            }
        }
    };

    var itEquals = function (first, second) {
        if (first && second) {
            if (first.val() !== second.val()) {
                first.addClass('error');
                second.addClass('error');
                passwordDiff(true);
            }
            else{
                first.removeClass('error');
                second.removeClass('error');
                passwordDiff(false);
            }
        }
    };

    var showErrors = function () {
        common.showErrors($('#errors'), 'Existen campos vacios!')
    };

    var passwordDiff = function (show) {
        if (show) {
            common.showErrors($('#errors_pass'), 'Las contraseñas no coinciden')
        }
    };

    return {
        init: init
    };

})();

registro.init();
