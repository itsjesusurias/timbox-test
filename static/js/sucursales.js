var sucursales = (function () {
    var init = function() {
        $('#registrar').on('click', validateFields);
    };

    var validateFields = function(event) {

        isValid(function () {
            $form = $('#form');
            if ($form.find('.error').length <= 0) {
                $form.submit();
            }else{
                showErrors();
            }
        });
    };

    var showErrors = function () {
        common.showErrors($('#errors'), 'Existen campos vacios!')
    };

    var isValid = function (callback) {
        var $nombre = $('#id_nombre'),
            $numeroInterior = $('#id_numero_interior'),
            $numeroExterior = $('#id_numero_exterior');
        common.isNullOrEmpty([$nombre, $numeroInterior, $numeroExterior]);
        common.isNumber([$numeroInterior, $numeroExterior]);

        callback();
    }

    return {
        init: init
    };

})();

sucursales.init();
