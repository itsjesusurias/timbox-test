var empleados = (function () {
    var init = function() {
        $('#registrar').on('click', validateFields);
    };

    var validateFields = function(event) {

        isValid(function () {
            $form = $('#form');
            if ($form.find('.error').length <= 0) {
                $form.submit();
            }else{
                showErrors();
            }
        });
    };

    var showErrors = function () {
        common.showErrors($('#errors'), 'Existen campos vacios!')
    };

    var isValid = function (callback) {
        var $nombre = $('#id_nombre'),
            $rfc = $('#id_rfc');
            $sucursal = $('#id_sucursal');
        common.isNullOrEmpty([$nombre, $rfc, $sucursal]);

        callback();
    }

    return {
        init: init
    };

})();

empleados.init();
