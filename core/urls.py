from django.conf.urls import patterns, include, url
from . import views

urlpatterns = [
    url(r'^acceder$', views.acceder, name="acceder"),
    url(r'^registro/$', views.registro, name="registro"),
    url(r'^salir/$', views.salir, name="salir"),
]
