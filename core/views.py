from django.shortcuts import render,redirect,render_to_response,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from .forms import UserForm, PersonaForm
from django.contrib.auth.hashers import make_password


#@login_required()
def acceder(request):
    context = RequestContext(request)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return redirect(reverse('empresa:index'))
            else:
                return HttpResponse("Cuenta desactivada.")
        else:
            message = "Datos incorrectos"
            return render(request,'account/login.html', locals())

    else:
        return render_to_response('account/login.html', {}, context)


def registro(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        profile_form = PersonaForm(request.POST)
        if user_form.is_valid() * profile_form.is_valid():
            user = user_form.save(commit=False)
            user.username = user.email
            user.password = make_password(user_form.cleaned_data['password'])
            user.save()
            user_profile = profile_form.save(commit=False)
            user_profile.user = user
            user_profile.save()
            return redirect(reverse('core:acceder'))
    else:
        user_form = UserForm()
        profile_form = PersonaForm()
    return render_to_response('account/registro.html', dict(user_form=user_form, profile_form=profile_form), context_instance=RequestContext(request))

@login_required
def salir(request):
    logout(request)
    return redirect(reverse('core:acceder'))
