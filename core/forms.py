from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import Persona

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        exclude = ['username', 'first_name', 'last_name', 'groups', 'user_permissions', 'is_staff', 'is_active', 'is_superuser', 'last_login', 'date_joined']

class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        exclude = ['user']
