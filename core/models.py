from django.db import models
from django.contrib.auth.models import User

class Persona(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    nombre_completo = models.CharField(max_length = 100)
    rfc = models.CharField(max_length = 20)
    empresa = models.CharField(max_length = 100)

    def __str__(self):
        return self.nombre_completo
